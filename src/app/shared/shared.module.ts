import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";

const routes: Routes = [
    { path: '', redirectTo: 'main', pathMatch: 'full' },
    { path: 'auth', loadChildren: '../pages/auth/auth.module#AuthPageModule' },
    { path: 'main', loadChildren: '../pages/main/main.module#MainPageModule' },
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ]
})
export class SharedModule { }
