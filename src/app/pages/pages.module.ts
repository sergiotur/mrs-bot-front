import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AuthPageModule} from "./auth/auth.module";
import {MainPageModule} from "./main/main.module";

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        AuthPageModule,
        MainPageModule
    ],
    exports: [
        AuthPageModule,
        MainPageModule
    ]
})
export class PagesModule {
}
